﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace WpfApp1.Components
{
    public class ZoomBorder : Border
    {
        private Point _origin;
        private Point _start;

        public override UIElement Child
        {
            get { return base.Child; }
            set
            {
                if (value != null && value != base.Child)
                    Initialize(value);
            }
        }

        private static TranslateTransform GetTranslateTransform(UIElement element) => (TranslateTransform) 
            ((TransformGroup)element.RenderTransform).Children.First(tr => tr is TranslateTransform);


        private static ScaleTransform GetScaleTransform(UIElement element) => (ScaleTransform)
            ((TransformGroup)element.RenderTransform).Children.First(tr => tr is ScaleTransform);
        


        public void Initialize(UIElement element)
        {
            base.Child = element;
            if (base.Child != null)
            {
                TransformGroup group = new();
                ScaleTransform st = new();
                TranslateTransform tt = new();

                group.Children.Add(st);
                group.Children.Add(tt);

                base.Child.RenderTransform = group;
                base.Child.RenderTransformOrigin = new(0.0, 0.0);

                MouseWheel += Child_MouseWheel;
                MouseLeftButtonDown += Child_MouseLeftButtonDown;
                MouseLeftButtonUp += Child_MouseLeftButtonUp;
                MouseMove += Child_MouseMove;
                PreviewMouseRightButtonDown += Child_PreviewMouseRightButtonDown;
            }
        }

        public void Reset()
        {
            if (base.Child != null)
            {
                // reset zoom
                var st = GetScaleTransform(base.Child);
                st.ScaleX = 1.0;
                st.ScaleY = 1.0;

                // reset pan
                var tt = GetTranslateTransform(base.Child);
                tt.X = 0.0;
                tt.Y = 0.0;
            }
        }

        #region Child Events

        private void Child_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (base.Child != null)
            {
                var st = GetScaleTransform(base.Child);
                var tt = GetTranslateTransform(base.Child);

                double zoom = e.Delta > 0 ? .2 : -.2;
                if (e.Delta <= 0 && (st.ScaleX < .4 || st.ScaleY < .4))
                    return;

                Point relative = e.GetPosition(base.Child);
                double absoluteX;
                double absoluteY;

                absoluteX = relative.X * st.ScaleX + tt.X;
                absoluteY = relative.Y * st.ScaleY + tt.Y;

                st.ScaleX += zoom;
                st.ScaleY += zoom;

                tt.X = absoluteX - relative.X * st.ScaleX;
                tt.Y = absoluteY - relative.Y * st.ScaleY;
            }
        }

        private void Child_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (base.Child != null)
            {
                var tt = GetTranslateTransform(base.Child);
                _start = e.GetPosition(this);
                _origin = new(tt.X, tt.Y);
                Cursor = Cursors.Hand;
                Child.CaptureMouse();
            }
        }

        private void Child_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (base.Child != null)
            {
                base.Child.ReleaseMouseCapture();
                Cursor = Cursors.Arrow;
            }
        }

        void Child_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e) => Reset();

        private void Child_MouseMove(object sender, MouseEventArgs e)
        {
            if (base.Child != null && base.Child.IsMouseCaptured)
            {
                var tt = GetTranslateTransform(base.Child);
                Vector v = _start - e.GetPosition(this);
                tt.X = _origin.X - v.X;
                tt.Y = _origin.Y - v.Y;

            }
        }

        #endregion
    }
}