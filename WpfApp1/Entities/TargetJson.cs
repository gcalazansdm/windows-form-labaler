﻿using Newtonsoft.Json;

namespace WpfApp1.Entities
{
    internal class TargetJson
    {
        [JsonProperty("Nota")]
        public readonly string Grade;

        [JsonProperty("C1")]
        public readonly string CompetenceOne;

        [JsonProperty("C2")]
        public readonly string CompetenceTwo;

        [JsonProperty("C3")]
        public readonly string CompetenceThree;

        [JsonProperty("C4")]
        public readonly string CompetenceFour;

        [JsonProperty("Text")]
        public readonly string Text;

        public TargetJson()
        {
            Grade = "";
            CompetenceOne = "";
            CompetenceTwo = "";
            CompetenceThree = "";
            CompetenceFour = "";
            Text = "";
        }

        private TargetJson(string grade, string competenceOne, string competenceTwo, string competenceThree, string competenceFour, string text)
        {
            Grade = grade;
            CompetenceOne = competenceOne;
            CompetenceTwo = competenceTwo;
            CompetenceThree = competenceThree;
            CompetenceFour = competenceFour;
            Text = text;
        }
        public static string GetJSON(string[] competence, string grade, string text)
        {
            TargetJson dataCollector = new(grade, competence[0], competence[1], competence[2], competence[3], text);
            return JsonConvert.SerializeObject(dataCollector);
        }
        public static TargetJson DecodeJSON(string myJson)
        {
            return JsonConvert.DeserializeObject<TargetJson>(myJson);
        }
    }
}
