﻿using Newtonsoft.Json;

namespace WpfApp1.Entities
{
    public class FullTextAnnotation
    {
        [JsonProperty("text")]
        public string Text { get; set; }
    }

    internal class GOCR
    {
        [JsonProperty("fullTextAnnotation")]
        private FullTextAnnotation fullTextAnnotation { get; set; }

        public string Text { get => fullTextAnnotation.Text; }
        public static GOCR DecodeJSON(string myJsonResponse) => JsonConvert.DeserializeObject<GOCR>(myJsonResponse);
    }
}
