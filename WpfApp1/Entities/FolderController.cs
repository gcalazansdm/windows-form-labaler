﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WpfApp1.Entities
{
    internal class FolderController
    {
        private readonly string _folderName;
        private readonly List<string> _files;
        private readonly List<string> _savedFiles;
        private readonly string _saveFolder;
        private readonly string _gocrFolder;

        private int _pos = 0;

        public string FolderName { get => _folderName; }

        public FolderController(string folderName)
        {
            _folderName = folderName;
            string[] extensions = new[] { "jpg", "jpeg", "png" };
            _files = Directory.GetFiles(folderName).Where(file => extensions.Any(file.ToLower().EndsWith)).ToList();
            _saveFolder = Path.Combine(_folderName, "Labels");
            _gocrFolder = Path.Combine(_folderName, "GOCR");

            if (!Directory.Exists(_saveFolder))
                Directory.CreateDirectory(_saveFolder);

            _savedFiles = new List<string>();
            foreach (string f in Directory.GetFiles(_saveFolder)) {
                string name = Path.GetFileNameWithoutExtension(f);
                _savedFiles.Add(name);
            }
        }

        public string GetNextFile() => _files[(_pos >= _files.Count - 1) ? _pos : ++_pos];
        public string GetPrevFile() => _files[(_pos <= 0) ? _pos : --_pos];
        public string GetFile() => _files[_pos];
        public string GetProgress() => $"Position: {_pos + 1}/Indexed: {_savedFiles.Count}/Total: {_files.Count}";

        public string SaveFile(string text)
        {
            string name = Path.GetFileNameWithoutExtension(_files[_pos]);
            if (!_savedFiles.Contains(name))
                _savedFiles.Add(name);
            string result = Path.Combine(_saveFolder, name + ".json");
            File.WriteAllTextAsync(result, text);
            return name;
        }

        public string GetGocr()
        {
            string name = Path.GetFileNameWithoutExtension(_files[_pos]);

            if (!_savedFiles.Contains(name))
                _savedFiles.Add(name);

            string result = Path.Combine(_gocrFolder, name + ".json");

            return File.Exists(result) ? GOCR.DecodeJSON(File.ReadAllText(result)).Text : "";

        }

        public TargetJson GetPrevJson()
        {
            string name = Path.GetFileNameWithoutExtension(_files[_pos]);
            
            if (!_savedFiles.Contains(name))
                _savedFiles.Add(name);

            string result = Path.Combine(_saveFolder, name + ".json");

            return File.Exists(result) ? TargetJson.DecodeJSON(File.ReadAllText(result)) : new TargetJson();

        }
    }
}
