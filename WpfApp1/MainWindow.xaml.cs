﻿using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using WpfApp1.Entities;
using Path = System.IO.Path;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FolderController _folderController;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SaveMethod(object sender, RoutedEventArgs e)
        {
            _folderController.SaveFile(TargetJson.GetJSON(new string[] { C1.Text, C2.Text, C3.Text, C4.Text }, Nota.Text, MainTextBox.Text));
            UpdateImage(_folderController.GetNextFile());
            UpdateTextBox();
        }
        private static string GetValue(string value) => string.IsNullOrEmpty(value) ? "" : value;
        private void UpdateTextBox()
        {
            TargetJson targetJson = _folderController.GetPrevJson();
            C1.Text = GetValue(targetJson.CompetenceOne);
            C2.Text = GetValue(targetJson.CompetenceTwo);
            C3.Text = GetValue(targetJson.CompetenceThree);
            C4.Text = GetValue(targetJson.CompetenceFour);
            Nota.Text = GetValue(targetJson.Grade);
            MainTextBox.Text = string.IsNullOrEmpty(targetJson.Text) ? _folderController.GetGocr() : targetJson.Text;
            TxtUsername.Text = _folderController.GetProgress();
        }

        private void IncresseMethod(object sender, RoutedEventArgs e)
        {
            MainTextBox.FontSize *= 1.5;
            Nota.FontSize *= 1.5;
            C1.FontSize *= 1.5;
            C2.FontSize *= 1.5;
            C3.FontSize *= 1.5;
            C4.FontSize *= 1.5;
        }
        private void DecresseMethod(object sender, RoutedEventArgs e)
        {

            MainTextBox.FontSize /= 1.5;
            Nota.FontSize /= 1.5;
            C1.FontSize /= 1.5;
            C2.FontSize /= 1.5;
            C3.FontSize /= 1.5;
            C4.FontSize /= 1.5;
        }

        private void NextMethod(object sender, RoutedEventArgs e)
        {
            _folderController.SaveFile(TargetJson.GetJSON(new string[] { C1.Text, C2.Text, C3.Text, C4.Text }, Nota.Text, MainTextBox.Text));
            UpdateImage(_folderController.GetNextFile());
            UpdateTextBox();

        }

        private void PrevMethod(object sender, RoutedEventArgs e)
        {
            _folderController.SaveFile(TargetJson.GetJSON(new string[] { C1.Text, C2.Text, C3.Text, C4.Text }, Nota.Text, MainTextBox.Text));
            UpdateImage(_folderController.GetPrevFile());
            UpdateTextBox();
        }

        private void LoadMethod(object sender, RoutedEventArgs e)
        {
            using var folderBrowserDialog = new FolderBrowserDialog();

            DialogResult result = folderBrowserDialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK || string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                return;

            _folderController = new(folderBrowserDialog.SelectedPath);

            LoadBox.Text = _folderController.FolderName;
            ButtonNext.IsEnabled = true;
            ButtonPrev.IsEnabled = true;
            UpdateImage(_folderController.GetFile());
            UpdateTextBox();
        }

        private void UpdateImage(string path)
        {
            Title.Text = Path.GetFileNameWithoutExtension(path);

            byte[] photoFile = File.ReadAllBytes(path);
            BitmapImage photo = new BitmapImage();

            using (var stream = new MemoryStream(photoFile))
            {
                photo.BeginInit();
                photo.CacheOption = BitmapCacheOption.OnLoad;
                photo.StreamSource = stream;
                photo.EndInit();
            }

            ImageLoaden.Source = photo;
            ImageBoarder.Reset();
        }

        private void MainTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {

        }
    }
}
